
import java.util.ArrayList;


public class Phonebook{
    private Contact contact;

     ArrayList<Contact> contacts = new ArrayList<Contact>();


    public Phonebook(){}

    public Phonebook(Contact contact){
        this.contact = contact;
    }


    public Contact getContact(){
        return contact;
    }


    public void setContact(Contact contact){
        contacts.add(contact);
    }


}
